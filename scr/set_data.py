import pandas as pd
import psycopg2 
from sqlalchemy import create_engine
import funcs

connection = funcs.get_con_string()
project_folder = str(__file__).strip()

df = pd.read_csv(funcs.get_path() + "/dump.csv", delimiter=';', quotechar='"')
df = df[["ip", "domain"]]
df["ip"] = df['ip'].str.split('|')
df = df.explode('ip')

engine = create_engine(connection)
df.to_sql('block_list', engine)