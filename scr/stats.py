import psycopg2
import sys
import funcs
import argparse
from io import TextIOWrapper

def print_statistics(statistics:dict, buffer:TextIOWrapper):
   for i, j in statistics.items():
      buffer.write(f'{i} : {j}\n')

queries = {
    'entries' : 'SELECT index FROM block_list ORDER BY index DESC LIMIT 1;',
    'ips' : 'SELECT COUNT(ip) FROM block_list;',
    'domains':'SELECT COUNT(domain) FROM block_list;',
}
statistics = {
    "entries" : 0,
    "ips" : 0,
    "domains" : 0,    
}
connection = funcs.get_con_string()
conn = psycopg2.connect(connection)
conn.autocommit = True
cursor = conn.cursor()
for key in queries.keys():
   cursor.execute(queries[key])
   statistics[key] = cursor.fetchall()[0][0]
cursor.close()
conn.close()

parser = argparse.ArgumentParser()
parser.add_argument('--file', '-f', help="output file for statistics")
args = parser.parse_args()
if args.file is None :
   out = TextIOWrapper(sys.stdout.buffer)
   print_statistics(statistics, out)
else:
   with open(args.file, '+w') as out:
      print_statistics(statistics, out)


