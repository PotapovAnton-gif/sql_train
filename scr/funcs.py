def get_path():
    targ = str(__file__)
    for i in range(2):
        index = targ.rfind('/') 
        targ = targ[:index]
    return targ

def get_con_string():
    with open(get_path()+'/creds/creds.txt') as file:
        conn = file.readline().strip('\n')
    return conn



