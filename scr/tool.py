#!/bin/python3

import psycopg2
import argparse
import funcs
import ipaddress


def tool():
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--ip', '-i', help="ip adress for checking in database")
    parser.add_argument('--domain', '-d', help="domain name for cheking in database")

    args = parser.parse_args()

    result = {'ip':None, 'domain':None}
    ip = '0'
    domain = '.'
    if args.ip is None and args.domain is None:
        parser.print_help()
    if args.ip is not None:
        ip = args.ip
    if args.domain is not None:
        domain = args.domain

    connection = funcs.get_con_string()
    conn = psycopg2.connect(connection)
    conn.autocommit = True
    cursor = conn.cursor()
    sql_query = f'''SELECT DISTINCT ip, domain FROM block_list WHERE (ip = '{ip}' OR domain = '{domain}');'''
    cursor.execute(sql_query)
    notes = cursor.fetchall()
    notes = [i for tup in notes for i in tup]
   
    if args.domain is not None and domain in notes:
        result['domain'] = True
   
    elif args.domain is not None:
        result['domain'] = False
   
    if args.ip is not None and ip in notes:
        result['ip'] = True
   
    elif args.ip is not None:
        in_block = False
        sql_query = "SELECT ip FROM block_list WHERE ip LIKE '%/%'";
        cursor.execute(sql_query)
        subnets = cursor.fetchall()
        subnets = [i for tup in subnets for i in tup]
        for subnet in subnets:
            if ipaddress.ip_address(ip) in ipaddress.ip_network(subnet):
                in_block = True
                result['ip'] = True
        if not in_block:
            result['ip'] = False 
    conn.close()

    return result

answer = tool()
print(answer)